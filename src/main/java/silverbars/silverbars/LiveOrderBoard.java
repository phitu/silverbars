package silverbars.silverbars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LiveOrderBoard {

	// group orders together based on order price and total quantity
	public List<Order> groupOrder(List<Order> liveOrders) {
	
		List<Order> groupedOrders = new ArrayList<>();
		
		for(Order order : liveOrders) {

			if(inOrders(groupedOrders, order)) {
				double totalExistingQuantity = groupedOrders.stream().filter(a -> a.getPrice() == order.getPrice() && a.getOrderType() == order.getOrderType()).mapToDouble(a->a.getQuantity()).sum();
				
				final double totalQuantity = totalExistingQuantity += order.getQuantity();

				List<Order> existingOrders = groupedOrders.stream()
						.filter(a -> a.getPrice() != order.getPrice() && a.getOrderType() == order.getOrderType())
						.collect(Collectors.toList());
				List<Order> newOrder = groupedOrders.stream()
						.filter(a -> a.getPrice() == order.getPrice() && a.getOrderType() == order.getOrderType())
						.peek(c->c.setQuantity(totalQuantity))
						.collect(Collectors.toList());
				groupedOrders.clear();
				groupedOrders.addAll(existingOrders);
				groupedOrders.addAll(newOrder);
			
			}
			else {
				groupedOrders.add(order);				
			}
		}
		
		return groupedOrders;
	}
	
	// check if order is currently in list
	private boolean inOrders(List<Order> orders, Order order) {
		
		long b = orders.stream().filter(a -> a.getPrice() == order.getPrice() && a.getOrderType() == order.getOrderType()).count();
		if (b > 0) {
			return true; 
		}
		else {
			return false;
		}
	}
	
	// order sell orders in ascending order
	public List<Order> orderedSellOrders(List<Order> registeredOrders) {
		List<Order> sellOrders = new ArrayList<>();
		for(Order order : registeredOrders) {
			if(order.getOrderType().equals(OrderType.SELL.name())) {
				sellOrders.add(order);
			}
		}
		
		Collections.sort(sellOrders, new Comparator<Order>() {

			public int compare(Order o1, Order o2) {
				Integer a = o1.getPrice();
				Integer b = o2.getPrice();
				return a.compareTo(b);
			}
		});
		
		return sellOrders;
	}

	// order buy orders in descending order
	public List<Order> orderedBuyOrders(List<Order> registeredOrders) {
		List<Order> buyOrders = new ArrayList<>();
		for(Order order : registeredOrders) {
			if(order.getOrderType().equals(OrderType.BUY.name())) {
				buyOrders.add(order);
			}
		}
		
		Collections.sort(buyOrders, new Comparator<Order>() {

			public int compare(Order o1, Order o2) {
				Integer a = o1.getPrice();
				Integer b = o2.getPrice();
				return b.compareTo(a);
			}
		});
		
		return buyOrders;
	}
}
