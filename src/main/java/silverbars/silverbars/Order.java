package silverbars.silverbars;

public class Order {
	private int orderNo;

	private User userId;

	private double quantity;

	private int price;

	private String orderType;

	public int getOrderNo() {
		return orderNo;
	}	

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}
	
	public User getUserId() {
		return userId;
	}	

	public void setUserId(User userId) {
		this.userId = userId;
	}
	
	public double getQuantity() {
		return quantity;
	}	

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public int getPrice() {
		return price;
	}	

	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getOrderType() {
		return orderType;
	}	

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}
