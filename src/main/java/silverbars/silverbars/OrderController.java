package silverbars.silverbars;

import java.util.ArrayList;
import java.util.List;

public class OrderController {

	private List<Order> orders = new ArrayList<>();
	
	public List<Order> getOrders() {
		return orders;
	}
	
	public void registerOrder(Order order) {
		orders.add(order);
	}
	
	public boolean cancelOrder(int orderNo) {
		for(Order order : orders) {
			if(order.getOrderNo() == orderNo) {
				orders.remove(order);
				return true;
			}
		}	
		return false;
	}	
}
