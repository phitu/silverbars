package silverbars.silverbars;

public enum OrderType {
	BUY("BUY"), SELL("SELL");
	
	String orderType;
	
	OrderType(String order) {
		orderType = order;
	}
}	
