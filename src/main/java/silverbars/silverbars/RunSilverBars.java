package silverbars.silverbars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RunSilverBars 
{
	UserController userController = new UserController();
	OrderController orderController = new OrderController();
	LiveOrderBoard orderBoard = new LiveOrderBoard();
	BufferedReader c = new BufferedReader(new InputStreamReader(System.in));
	
	// create users
	public void createUsers() {
		userController.addUser(new User("User 1"));
		userController.addUser(new User("User 2"));
		userController.addUser(new User("User 3"));		
		userController.addUser(new User("User 4"));
	}
	
	// display main menu
	public void displayMenu() throws IOException {
		System.out.println("Enter 1 to register an order");
		System.out.println("Enter 2 to cancel an order");
		System.out.println("Enter 3 for live trade board");
		String menu = c.readLine();
		switch(menu) {
			case "1" : registerOrder();
				break;
			case "2" : cancelOrder();
				break;
			case "3" : displayLiveTradeBoard();
				break;
			default : System.out.println("Command not recognised");
				displayMenu();
		}
	}
	
	private void registerOrder() throws IOException {
		try {
			System.out.println("Enter user id-");
			String userInput = c.readLine();
			User user = userController.findUser(userInput);
			if (user == null){ 
				System.out.println("User not found");
				registerOrder();
			}
			System.out.println("Order quantity ('BUY' or 'SELL')-");
			String quantity = c.readLine();
			System.out.println("Price per kg-");
			String price = c.readLine();
			System.out.println("Order Type-");
			String orderType = c.readLine();
			
			Order order = new Order();
			order.setUserId(user);
			order.setQuantity(Double.parseDouble(quantity));
			order.setPrice(Integer.parseInt(price));
			if(orderType.equals("BUY"))
				orderType = OrderType.BUY.toString();
			else if(orderType.equals("SELL"))
				orderType = OrderType.SELL.toString();
			else 
				throw new Exception();
			order.setOrderType(orderType);
			int orderNo = orderController.getOrders().size() == 0 ? 1 : orderController.getOrders().stream().mapToInt(Order::getOrderNo).max().getAsInt() + 1;
			order.setOrderNo(orderNo);
			orderController.registerOrder(order);
			System.out.println("Registered Successfully");
		} catch(Exception e) {
			System.out.println("issue with input data, try again");
			registerOrder();
		}
		displayMenu();
	}
	
	private void cancelOrder() throws IOException {
		System.out.println("Enter order id-");
		String orderInput = c.readLine();
		boolean orderFound = orderController.cancelOrder(Integer.parseInt(orderInput));
		if(orderFound == true) {
			System.out.println("Order found and cancelled");
		}
		else {
			System.out.println("Order not found");
		}		
		displayMenu();
	}
	
	private void displayLiveTradeBoard() throws IOException {	
		
		List<Order> groupedOrders = orderBoard.groupOrder(orderController.getOrders());
		List<Order> sellOrders = orderBoard.orderedSellOrders(groupedOrders);
		List<Order> buyOrders = orderBoard.orderedBuyOrders(groupedOrders);
		printOrders(sellOrders);
		printOrders(buyOrders);
		c.readLine();
		displayMenu();
	}
	
	// print orders to user
	private void printOrders(List<Order> orderList) {
		orderList.forEach(order -> System.out.println(order.getOrderType() + ": " + order.getQuantity() + " kg for £" + order.getPrice() + " [" + order.getUserId().getUserId() + "] - Order ID - " + order.getOrderNo()));
		
	}

	public static void main(String[] args) throws IOException {
		RunSilverBars app = new RunSilverBars();
		app.createUsers();
		app.displayMenu();
	}
}
