package silverbars.silverbars;

public class User {

	private String userId;

	public User(String user) {
		this.userId = user;
	}
	
	public String getUserId() {
		return userId;
	}	

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
