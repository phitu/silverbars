package silverbars.silverbars;

import java.util.ArrayList;
import java.util.List;

public class UserController {

	private List<User> userList = new ArrayList<>();
	
	public List<User> getUsers() {
		return userList;
	}
	
	public void addUser(User user) {
		userList.add(user);
	} 
	
	public User findUser(String userId) {
		for(User user : userList) {
			if(user.getUserId().equals(userId)) {
				return user;
			}
		}
		return null;
	} 
}
