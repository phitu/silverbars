package silverbars.silverbars;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class LiveOrderBoardTest {

	private LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
	private OrderController orderController = new OrderController();
	
	private Order order1;
	
	private Order order2;
	
	private Order order3;
	
	private Order order4;
	
	private Order order5;
	
	private Order order6;
	
	private Order order7;
	
	private static final int ORDER_NO_1 = 1;
	
	private static final int ORDER_NO_2 = 2;
	
	private static final int ORDER_NO_3 = 3;
	
	private static final int ORDER_NO_4 = 4;
	
	private static final int ORDER_NO_5 = 5;
	
	private static final int ORDER_NO_6 = 6;
	
	private static final int ORDER_NO_7 = 7;
	
	@Before
	public void setUp() {
		order1 = new Order();
		order1.setOrderNo(ORDER_NO_1);
		order1.setQuantity(50);
		order1.setPrice(200);
		order1.setOrderType(OrderType.SELL.toString());
		
		order2 = new Order();
		order2.setOrderNo(ORDER_NO_2);
		order2.setQuantity(120);
		order2.setPrice(100);
		order2.setOrderType(OrderType.SELL.toString());
		
		order3 = new Order();
		order3.setOrderNo(ORDER_NO_3);
		order3.setQuantity(30);
		order3.setPrice(400);
		order3.setOrderType(OrderType.SELL.toString());
		
		order4 = new Order();
		order4.setOrderNo(ORDER_NO_4);
		order4.setQuantity(150);
		order4.setPrice(200);
		order4.setOrderType(OrderType.SELL.toString());
		
		order5 = new Order();
		order5.setOrderNo(ORDER_NO_5);
		order5.setQuantity(80);
		order5.setPrice(111);
		order5.setOrderType(OrderType.BUY.toString());
		
		order6 = new Order();
		order6.setOrderNo(ORDER_NO_6);
		order6.setQuantity(10);
		order6.setPrice(250);
		order6.setOrderType(OrderType.BUY.toString());
		
		order7 = new Order();
		order7.setOrderNo(ORDER_NO_7);
		order7.setQuantity(180);
		order7.setPrice(200);
		order7.setOrderType(OrderType.BUY.toString());

		orderController.registerOrder(order1);
		orderController.registerOrder(order2);
		orderController.registerOrder(order3);
		orderController.registerOrder(order4);
		orderController.registerOrder(order5);
		orderController.registerOrder(order6);
		orderController.registerOrder(order7);
	}
	
	@Test
	public void registerOrderSuccess() {
		
		List<Order> grouped = liveOrderBoard.groupOrder(orderController.getOrders());

		assertThat(grouped.size(), is(6));
	}
	
	@Test
	public void orderedSellOrders() {
		
		List<Order> grouped = liveOrderBoard.groupOrder(orderController.getOrders());
		List<Order> orderedSell = liveOrderBoard.orderedSellOrders(grouped);

		assertThat(orderedSell.size(), is(3));
		assertThat(orderedSell.get(0).getPrice(), is(100));
	}
	
	@Test
	public void orderedBuyOrders() {
		
		List<Order> grouped = liveOrderBoard.groupOrder(orderController.getOrders());
		List<Order> orderedSell = liveOrderBoard.orderedBuyOrders(grouped);

		assertThat(orderedSell.size(), is(3));
		assertThat(orderedSell.get(0).getPrice(), is(250));
	}

}
