package silverbars.silverbars;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class OrderControllerTest {

	private OrderController orderController = new OrderController();
	
	private Order order;
	
	private static final int ORDER_NO = 1;
	
	@Before
	public void setUp() {
		order = new Order();
		order.setOrderNo(ORDER_NO);
	}
	
	@Test
	public void registerOrderSuccess() {
		
		orderController.registerOrder(order);
		
		assertThat(orderController.getOrders().size(), is(1));
		assertThat(orderController.getOrders().get(0).getOrderNo(), is(ORDER_NO));
	}
	
	@Test
	public void cancelOrderSuccess() {
		
		orderController.registerOrder(order);
		boolean cancelledOrder = orderController.cancelOrder(ORDER_NO);
		assertThat(orderController.getOrders().size(), is(0));
		assertThat(cancelledOrder, is(true));
	}
	
	@Test
	public void cancelOrderUnsuccessful() {
		
		boolean cancelledOrder = orderController.cancelOrder(ORDER_NO);
		assertThat(orderController.getOrders().size(), is(0));
		assertThat(cancelledOrder, is(false));
	}

}
