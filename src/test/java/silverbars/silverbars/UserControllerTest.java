package silverbars.silverbars;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class UserControllerTest {

	private UserController userController = new UserController();
	
	@Test
	public void getUserSuccessful() {
		String userId = "User a";
		userController.addUser(new User(userId));
		
		assertThat(userController.getUsers().size(), is(1));
		assertThat(userController.getUsers().get(0).getUserId(), is(userId));
	}
	
	@Test
	public void findUserSuccessful() {

		String userId = "User a";
		userController.addUser(new User(userId));
		
		User user = userController.findUser(userId);
		
		assertThat(user.getUserId(), is(userId));
	}
	

	@Test
	public void findUserUnsuccessful() {

		String userId = "User a";
		userController.addUser(new User(userId));
		
		User user = userController.findUser("fake user");
		
		assertNull(user);
	}
}
